<?php 

class CommentBarFrontEnd{
	
	function __construct(){
		//add_action('wp_footer', array(&$this, 'include_comment_bar'));
		add_action('wp_enqueue_scripts', array(&$this, 'include_scripts'));
		add_filter('the_content', array(&$this, 'add_comment_bar'));
		
		//form submission
		add_action('init', array(&$this, 'comment_bar_submitted'));
		
		//remove default comment
		add_filter('comments_open', array(&$this, 'is_comment_open'), 100, 2);
	}
	
	
	//if comment bar is on, remove the default comment section
	function is_comment_open($open, $post_id){
		global $commentbar;
		if($commentbar->options->is_commentbar_active($post_id)){
			$open = false;
		}
		return $open;
	}
	
	
	//include the comment bar in footer sectio n
	function include_comment_bar(){
		if(is_page() || is_single()):
			global $commentbar, $post;
			if($commentbar->options->is_commentbar_active($post->ID)){
				include $commentbar->get_this_dir() . 'includes/front-end.php';
			}
		endif;
	}
	
	
	//include scripts
	function include_scripts(){
		if(is_page() || is_single()):
			global $commentbar, $post;
			if($commentbar->options->is_commentbar_active($post->ID)){
				if($this->already_commented($post->ID)){
					wp_register_style('commentbar-front-end-css', $commentbar->get_this_url() . 'css/front-end_2.css');
					wp_enqueue_style('commentbar-front-end-css');
				}
				else{
					wp_register_style('commentbar-front-end-css', $commentbar->get_this_url() . 'css/front-end.css');
					wp_enqueue_style('commentbar-front-end-css');
				}
				
				//javascript
				wp_register_script('comment-bar-auto-resize', $commentbar->get_this_url() . 'js/autoresize.js', array('jquery'));
				wp_enqueue_script('comment-bar-auto-resize');
			
				//enqueue the howler.js
				wp_register_script('blip_sound_maker', $commentbar->get_this_url() . 'js/howler-player/howler.min.js', array('jquery'), 2.00, true);
				wp_enqueue_script('blip_sound_maker');
			}
		endif;
	}
	
	//add comment bar
	function add_comment_bar($content){
		if(is_page() || is_single()){
			global $commentbar, $post;	
			if($commentbar->options->is_commentbar_active($post->ID)){
				if($this->already_commented($post->ID)){
					include $commentbar->get_this_dir() . 'includes/front-end_2.php';
				}
				else{		
					include $commentbar->get_this_dir() . 'includes/front-end.php';
				}			
				$content .= $com;
			}
		}
		
		return $content;
	}
	
	
	//boolean checking if there is a comment posted from this computer
	function already_commented($post_id){
		$cookie_name = "wp_sub_command_" . $post_id; //unique cookie name
		if(isset($_COOKIE[$cookie_name]) && $_COOKIE[$cookie_name] == $post_id){
			return true;
		}
		else{
			return false;
		}
	}
	
	
	//comment bar submitted
	function comment_bar_submitted(){
		global $commentbar;
		if(isset($_POST['comment_bar_submitted']) == 'y'){			
			
			$args = array();
			$args['comment_post_ID'] = (int) $_POST['comment_post_ID'];
			$args['comment_author'] = $_POST['commenter_name'];
			$args['comment_author_email'] = $_POST['commenter_email'];
			$args['comment_author_url'] = $_POST['commenter_website'];
			$args['comment_content'] = $_POST['comment_text'];
			$args['comment_author_IP'] = isset( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'] ;
			$args['comment_agent'] = $_SERVER['HTTP_USER_AGENT'];
			
			if($commentbar->options->is_auto_approved()){
				$approved = $commentbar->captcha->verify_cpatcha($_POST['comment_captcha']);				
				if($approved){
					add_filter('pre_comment_approved', array($this, 'pre_comment_approved'));
				}
				else{
					return;
				}
			}			
			
			
			if(is_user_logged_in()){
				$args['user_id'] = get_current_user_id();
			}
			
			$comment_id = wp_new_comment($args);
			
			//options
			$options = $commentbar->options->get_options();
			
			if(isset($_POST['checked_for_newsletter'])){
				//now autoresponders emails
				$emails = $this->extract_emails($options['autoresponder']);				
				
				if(!empty($emails)){
					$subject = "Newsletter";
					$body = 'autosubscriber';
					$headers[] = 'From: '.$args['comment_author'].' <'.$args['comment_author_email'].'>' . "\r\n";
					
					$commentbar->send_email($emails, $subject, $body, $headers);
					
					//set cookie to trace
					$this->keep_trace_on_comment($args);
					
				}
				
				//now redirection
				if($commentbar->options->is_redirect_enabled()){					
					//add_action('wp_footer', array(&$this, 'do_redirection_new_tab'));					
					$redirect_url = $options['redirect_url'];
					if($redirect_url){
						return $commentbar->redirect($redirect_url);
					}
				
				}
				
			}
			
			
		}
	}
	
	
	/**
	 * Approve a comment is captcha is true
	 * */
	function pre_comment_approved($approved){
		$approved = 1;
		return $approved;
	}
	
	
	/**
	 * Javascript redirection 
	 * */
	function do_redirection_new_tab(){
		global $commentbar;
		
		$options = $commentbar->options->get_options();
		$redirect_url = $options['redirect_url'];
		if($redirect_url){
			
			?>
			<script>
				jQuery(document).ready(function(evt){
					var url = "<?php echo $redirect_url; ?>";
										
				});
			</script>
			<?php 
			
			//return $commentbar->redirect($redirect_url);
		}			
	}
	
	
	/**
	 * Keep trace on comment
	 * @arg post parameters
	 * */
	function keep_trace_on_comment($args){
		$cookie_name = "wp_sub_command_" . $args['comment_post_ID']; //unique cookie name
		$expire = current_time('timestamp') + (365 * 86400); // 1 year
		return setcookie($cookie_name, $args['comment_post_ID'], $expire, '/');
	}
	
	
	//extract emails
	function extract_emails($string){
		$emails = array();
		
		$e = explode("\n", $string);
		if($e){
			foreach ($e as $email){
				$email = trim($email);
				if(is_email($email)){
					$emails[] = $email;
				}
			}
		}
		
		return $emails;
	}
	
}

?>
