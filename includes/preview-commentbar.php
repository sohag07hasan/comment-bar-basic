<?php 

$options = $commentbar->options->get_options();

$options['affiliate'] = strlen($options['affiliate']) > 5 ? $options['affiliate'] : $commentbar->options->get_default_affiliate();

$border = $commentbar->options->is_border_styling_enabled() ? '1px solid #' . $options['border'] : '';

$submit_button = '';
if($commentbar->options->is_submit_button_styler_enabled()){
	$submit_button .= 'background: #'. $options['submit_button'] . ' !important ; ';
	$submit_button .= 'color: #'. $options['submit_color'] . ' !important ;';
}


//captcha code inclusion
if($commentbar->options->is_auto_approved()){
	$captcha_image = $commentbar->captcha->get_captcha();
	$captcha = '<br/><span style=""> Calculate <span style="font-weight: bold; font-size: 1.2em;">'.$captcha_image.' <input name="comment_captcha" style="width: 2em; height: 1.3em;" type="text"></span></span>';
}
else{
	$captcha = '';
}

$style = '<style>
			
		div#commentbar{
			'.stripcslashes($options['css']).'
			border: '.$border.';
			color: #'.stripcslashes($options['text_color']).';		
		}
		
		input#commentbar_submit_button{
			cursor: pointer;
			cursor: hand;
			'.$submit_button.'
		}
		
		</style>';

$body = '<form class="comment_bar_form" action="" method="post">';

$body .= '
<div id="commentbar">
	
	<p class="heading-text">' . $options['required_text'] . '</p>'; 

if(strlen($options['thumbnail']) > 5){

	$body .= '<div class="comment_bar_thumb_container inside_commentbar"><img title="wp sub command" alt="" class="commbent_bar_thumb" src="' . $options['thumbnail'] . '" /> </div>';
}
else{
	$new_style = 'width: 90%;';
}
	
$body .=	'

	<div style="'.$new_style.'" class="comment_bar_required_text_container inside_commentbar">
		<p>Name: <input name="commenter_name" type="text" > Email: <input name="commenter_email" type="text"> </p>		
		<p>Website: <br/><input id="commenter_website_inputbox" type="text" name="commenter_website"></p>
		<p>Comment:<br/> <textarea id="comment_bar_textarea" name="comment_text" rows="1" cols="50" class="comment-box"></textarea></p>
		<p class="ending-text">
			<span><input name="checked_for_newsletter" type="checkbox" checked /> Yes! I want to be added to the newsletter to get my FREE gift. </span>		 		
			'.$captcha.'
			<span class="submit-container"><input id="commentbar_submit_button" type="submit" value="Submit"> <a target="_blank" title="Wp Sub Command" href="'.$options['affiliate'].'"> Powered by WP Sub Command! </a> </span>
		</p>
	</div>
	<div style="clear:both"></div>
				
</div>';

$body .= '</form>';

$com = $style . $body;

echo $com;


?>

<script>
	jQuery('#commentbar_submit_button').click(function(){return false;});
</script>
