<?php 
session_start();
/**
 * This class handles captcha 
 * */
class CommentbarCaptcha{
	
	function __construct(){
		
	}
	
	function get_captcha(){
		
		return $this->get_captcha_new();
		
		$width = 50;
		$height = 25;
		$len = 6; // you can change this
		mt_srand(time());
		// generate random values
		$r = 255;
		$g = 255;
		$b = 255;
		
		$s = 0;
		$h = 0;
		$c = 0;
		$s = mt_rand(0, 80);
		$h = mt_rand(80, 80);
		$c = mt_rand(80, 100);
		
		//code
		$code = mt_rand(100000,999999);
		$_SESSION['captcha'] = $code;
		
		$size = 0.75 * 40;
		$image = imagecreate($width, $height) or die("couldn't generate image");
		
		$bg = imagecolorallocate($image, $s, $h, $c);
		$c1 = imagecolorallocate($image, $r, $g, $b);
		
		return imagestring($image,2,3,3,$code,$c1);
		
		//header('Content-Type: image/png');
		//imagepng($image);
		
	
		imagedestroy($image);
	}
	
	function get_captcha_new(){
		$this->destroy_captcha();
		$a = rand(1, 9);
		$b = rand(1, 9);
		$code = $a + $b;
		$_SESSION['captcha']['code'] = $code;
		return "$a" . ' + ' . "$b = ";
	}
	
	function verify_cpatcha($code){
		return $_SESSION['captcha']['code'] == $code ? true : false;
	}
	
	function destroy_captcha(){
		unset($_SESSION['captcha']);
	}
	
}

?>